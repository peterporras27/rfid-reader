@extends('admin')

@section('content')

<div class="panel panel-default panel-shadow" data-collapsed="0" style="direction: ltr;">
	
	<div class="panel-heading">
		<div class="panel-title">Devices List<small class="m-l-sm"> Manage all RFID scanner devices.</small></div>
		
		<div class="panel-options"></div>
	</div>
	
	<!-- panel body -->
	<div class="panel-body">

		<form action="{{ route('devices.index') }}" method="GET" class="form-inline" role="form" style="direction: rtl;">

			<div class="input-group">
				<span class="input-group-addon">Per page</span> <input type="number" name="show" style="width: 80px;" placeholder="10" value="{{ $perpage }}" class="form-control">
			</div>

			<div class="input-group">
				<input type="text" name="search" value="{{ $search }}" placeholder="Name" class="form-control">
				<span class="input-group-btn"> 
					<button type="submit" class="btn btn-info" style="margin: 0;">Search <i class="fa fa-search"></i></button>
				</span>
			</div>

			<div class="input-group pull-right">
        		<a href="{{ route('devices.index') }}" class="btn btn-white">
					<i class="fa fa-refresh"></i> Reset Search
				</a>
			</div>
		</form>
		<hr>
		
		@if( $devices->count() )
			
		<div class="table-responsive">
			<table class="table table-bordered table-striped table-hover">
				<thead>
					<tr>
						<th><strong>Device Name</strong></th>
						<th><strong>IP Address</strong></th>
						<th><strong>Device Usage</strong></th>
						<th><strong>Options</strong></th>
					</tr>
				</thead>
				<tbody>
					@foreach( $devices as $device )
						<tr id="user-row-{{ $device->id }}">
							<td>{{ $device->name }}</td>
							<td>{{ $device->ip_address }}</td>
							<td>{{ $type[$device->type] }}</td>
							<td>
								<a href="{{ url("devices/{$device->id}/edit") }}" class="btn btn-success btn-xs">
									<i class="fa fa-edit"></i> Edit
								</a>
								<button data-id="{{ $device->id }}" data-name="delete" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#remove-device">
									<i class="fa fa-close"></i> Remove
								</button>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
		{{ $devices->appends( request()->input() )->links() }}
		@else
		<div class="alert alert-info">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<strong>No data found,</strong> you may now start adding new devices.
		</div>
		@endif
	</div>
</div>

<div class="modal inmodal" id="remove-device" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content animated fadeIn">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Remove Member</h4>
            </div>
            <div class="modal-body">
                <p><strong>Are you sure?</strong> once removed, his/her account and all data affiliated to this account will be deleted.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="pull-right btn btn-white"  data-dismiss="modal">Cancel</button>

                <button type="button" id="remove-btn" class="btn btn-danger">Remove</button>
            </div>
        </div>
    </div>
</div>

@endsection

@section('action')
<div class="title-action">
    <a href="{{ route('devices.create') }}" class="btn btn-primary">Add Device <i class="fa fa-plus"></i></a>
</div>
@endsection

@section('styles')
<!-- Toastr style -->
<link href="{{asset('css/plugins/toastr/toastr.min.css')}}" rel="stylesheet">
<style>
.idbox .underline {
    border-bottom: 1px solid #2076a5;
    font-size: 18px;
    color:  #2076a5 !important;
}
.idbox .date{
    width:25%;
    text-align: center;
    padding:15px;
    border: 1px solid #2076a5;
    float:left;
    color:  #2076a5 !important;
    margin-bottom: 23px;
}

.idbox h5, .idbox h4{margin:0; color: #fff !important}
.idbox strong, .idbox p, .idbox i{color:  #2076a5 !important;}
</style>
@endsection

@section('scripts')
<!-- Toastr script -->
<script src="{{asset('js/plugins/toastr/toastr.min.js')}}"></script>
<script>
jQuery(document).ready(function() {

	toastr.options = {
		"closeButton": true,
		"debug": false,
		"progressBar": true,
		"positionClass": "toast-top-right",
		"onclick": null,
		"showDuration": "400",
		"hideDuration": "1000",
		"timeOut": "7000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
	}

	jQuery('button[data-name="delete"]').click(function(e) {
		e.preventDefault();
		var token = jQuery('meta[name="csrf-token"]').attr('content');
	});

	jQuery('#remove-device').on('show.bs.modal', function (event) {
		var button = jQuery(event.relatedTarget); // Button that triggered the modal
		var user_id = button.data('id');
		jQuery('#remove-btn').data('id',user_id);
  		jQuery(this).find('.modal-title').text('Remove ' + jQuery('#user-row-'+user_id+' td:first').html() );
	});

	jQuery('#remove-btn').click(function(e) {
		e.preventDefault();
		var user_id = jQuery(this).data('id');
		var token = jQuery('meta[name="csrf-token"]').attr('content');

		jQuery.ajax({  
			url: '{{ route('devices.index') }}/'+user_id,
			type: 'POST',
			dataType: 'json',
			data: { _token: token, _method: 'DELETE' },
		})
		.always(function(data) {
			if (data.error) {
				toastr.error(data.message,'Error');
			} else {
				jQuery('#remove-device').modal('toggle');
				toastr.success(data.message,'Success');
				jQuery('#user-row-'+user_id).remove();
			}
		});
		 
	});
});
</script>
@endsection
