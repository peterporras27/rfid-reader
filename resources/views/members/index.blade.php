@extends('admin')

@section('content')

<div class="panel panel-default panel-shadow" data-collapsed="0" style="direction: ltr;">
	
	<div class="panel-heading">
		<div class="panel-title">User List<small class="m-l-sm"> Manage all user accounts.</small></div>
		
		<div class="panel-options"></div>
	</div>
	
	<!-- panel body -->
	<div class="panel-body">

		<form action="{{ route('members.index') }}" method="GET" class="form-inline" role="form" style="direction: rtl;">

			<div class="input-group">
				<span class="input-group-addon">Per page</span> <input type="number" name="show" style="width: 80px;" placeholder="10" value="{{ $perpage }}" class="form-control">
			</div>

			<div class="input-group">
				<input type="text" name="search" value="{{ $search }}" placeholder="Name" class="form-control">
				<span class="input-group-btn"> 
					<button type="submit" class="btn btn-info" style="margin: 0;">Search <i class="fa fa-search"></i></button>
				</span>
			</div>

			<div class="input-group pull-right">
        		<a href="{{ route('members.index') }}" class="btn btn-white">
					<i class="fa fa-refresh"></i> Reset Search
				</a>
			</div>
		</form>
		<hr>
		
		@if( $members->count() )
			
		<div class="table-responsive">
			<table class="table table-bordered table-striped table-hover">
				<thead>
					<tr>
						<th><strong>First Name</strong></th>
						<th><strong>Last Name</strong></th>
						<th><strong>Middle Name</strong></th>
						<th><strong>Status</strong></th>
						<th><strong>Card Number</strong></th>
						<th><strong>Options</strong></th>
					</tr>
				</thead>
				<tbody>
					@foreach( $members as $member )

						<tr id="user-row-{{ $member->id }}">
							<td>{{ $member->first_name }}</td>
							<td>{{ $member->last_name }}</td>
							<td>{{ $member->middle_name }}</td>
							<td>{{ $member->active ? 'Active':'Inactive' }}</td>
							<td>
								<div class="form-group rfid-inputs">
									<div class="input-group" style="direction: rtl;">
										<span class="input-group-btn"> 
											<button type="submit" data-id="{{ $member->id }}" class="btn btn-info scan-card" style="margin: 0;">Scan RFID</button>
										</span>
										<input id="card_number_{{ $member->id }}" style="direction: ltr;" type="text" value="{{ $member->card_number }}" class="form-control" disabled>
									</div>
								</div>
							</td>
							<td>
								<a href="{{ url("members/{$member->id}/edit") }}" class="btn btn-success btn-xs">
									<i class="fa fa-edit"></i> Edit
								</a>
								
								<button data-id="{{ $member->id }}" data-name="delete" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#remove-user">
									<i class="fa fa-close"></i> Remove
								</button>
								
							</td>
						</tr>

					@endforeach
				</tbody>
			</table>
		</div>
		{{ $members->appends( request()->input() )->links() }}
		@else
		<div class="alert alert-info">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<strong>No data found,</strong> you may now start adding new members.
		</div>
		@endif
	</div>
</div>

<div class="modal inmodal" id="remove-user" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content animated fadeIn">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Remove Member</h4>
            </div>
            <div class="modal-body">
                <p><strong>Are you sure?</strong> once removed, his/her account and all data affiliated to this account will be deleted.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="pull-right btn btn-white"  data-dismiss="modal">Cancel</button>

                <button type="button" id="remove-btn" class="btn btn-danger">Remove</button>
            </div>
        </div>
    </div>
</div>

@endsection

@section('action')
<div class="title-action">
	@if ( Auth::user()->hasRole('admin') )
    <a href="{{ route('members.create') }}" class="btn btn-primary">Add Member <i class="fa fa-plus"></i></a>
    @endif
</div>
@endsection

@section('styles')
<!-- Toastr style -->
<link href="{{asset('css/plugins/toastr/toastr.min.css')}}" rel="stylesheet">
<style>
.idbox .underline {
    border-bottom: 1px solid #2076a5;
    font-size: 18px;
    color:  #2076a5 !important;
}
.idbox .date{
    width:25%;
    text-align: center;
    padding:15px;
    border: 1px solid #2076a5;
    float:left;
    color:  #2076a5 !important;
    margin-bottom: 23px;
}

.idbox h5, .idbox h4{margin:0; color: #fff !important}
.idbox strong, .idbox p, .idbox i{color:  #2076a5 !important;}
</style>
@endsection

@section('scripts')
<!-- Toastr script -->
<script src="{{asset('js/plugins/toastr/toastr.min.js')}}"></script>
<script>
var cond = false;
var timer = null;
var activeID = null;
var activeBtn = null;

jQuery(document).ready(function() {

	toastr.options = {
		"closeButton": true,
		"debug": false,
		"progressBar": true,
		"positionClass": "toast-top-right",
		"onclick": null,
		"showDuration": "400",
		"hideDuration": "1000",
		"timeOut": "7000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
	}

	jQuery('.scan-card').click(function (e) {

		cond = false;
		
		var token = jQuery('meta[name="csrf-token"]').attr('content');
		activeBtn = jQuery(this);
		activeID = jQuery(this).data('id');
		e.preventDefault();
		var d = 'Scan new card';
		$(this).prop('disabled',true);
		$(this).addClass('disabled');
		$(this).html('Waiting for card to be scanned...');

		jQuery.ajax({  
			url: '/save-member/'+activeID,
			type: 'POST',
			dataType: 'json',
			data: { _token: token, _method: 'POST' },
		})
		.always(function(e) {

			console.log(e);

			if (e.error) {
				toastr.error(e.message,'Error');
			} else {
				
				timer = setInterval(function(){

					if( cond ) { clearInterval(timer); return;}

					jQuery.ajax({
						url: 'user-info/'+activeID,
						type: 'get',
						data: {},
						success: function (f) {
							console.log(f);

							if (f.error) {

								cond = false;

							} else {

								cond = true;
								activeBtn.prop('disabled',false);
								activeBtn.removeClass('disabled');
								activeBtn.html('Scan RFID');
								toastr.success(f.message,'Success');
								if (f.data.card_number) {
									$('#card_number_'+activeID).val(f.data.card_number);
								} else {
									$('#card_number_'+activeID).val('');
								}
							}
						}
					})

				}, 2000);
			}
		});
	});

	jQuery('button[data-name="delete"]').click(function(e) {
		e.preventDefault();
		var token = jQuery('meta[name="csrf-token"]').attr('content');
	});

	jQuery('#remove-user').on('show.bs.modal', function (event) {
		var button = jQuery(event.relatedTarget); // Button that triggered the modal
		var user_id = button.data('id');
		jQuery('#remove-btn').data('id',user_id);
  		jQuery(this).find('.modal-title').text('Remove ' + jQuery('#user-row-'+user_id+' td:first').html() );
	});

	jQuery('#remove-btn').click(function(e) {
		e.preventDefault();
		var user_id = jQuery(this).data('id');
		var token = jQuery('meta[name="csrf-token"]').attr('content');

		jQuery.ajax({  
			url: '{{ route('members.index') }}/'+user_id,
			type: 'POST',
			dataType: 'json',
			data: { _token: token, _method: 'DELETE' },
		})
		.always(function(data) {
			if (data.error) {
				toastr.error(data.message,'Error');
			} else {
				jQuery('#remove-user').modal('toggle');
				toastr.success(data.message,'Success');
				jQuery('#user-row-'+user_id).remove();
			}
		});
		 
	});
});

</script>
@endsection
