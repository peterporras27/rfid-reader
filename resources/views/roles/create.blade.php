@extends('admin')

@section('content')

<div class="panel panel-default panel-shadow" data-collapsed="0" style="direction: ltr;">
	
	<div class="panel-heading">
		<div class="panel-title">Add Role</div>
		
		<div class="panel-options"></div>
	</div>
	
	<!-- panel body -->
	<div class="panel-body">
		<form action="{{ route('roles.store') }}" method="POST" role="form">
			{{ csrf_field() }}

			<div class="row">
				<div class="col-md-6">

					<div class="form-group">
						<label for="username">Name</label>
						<input type="text" value="{{ old('name') }}" name="name" class="form-control" id="username" placeholder="Name">
					</div>

					<div class="form-group">
						<label for="description">Description</label>
						<textarea name="description" rows="5" class="form-control" id="description" placeholder="Description">{{ old('description') }}</textarea>
					</div>

					<button type="submit" class="btn btn-primary">Save Role <i class="fa fa-save"></i></button>

				</div>

			</div>
		
		</form>
	</div>
</div>

@endsection

@section('action')
<div class="title-action">
<a href="{{ route('roles.index') }}" class="btn btn-white">
	Back <i class="fa fa-chevron-left"></i> 
</a>
</div>
@endsection


@section('styles')
<link href="{{asset('css/plugins/iCheck/custom.css')}}" rel="stylesheet">
@endsection

@section('scripts')
<!-- iCheck -->
<script src="{{asset('js/plugins/iCheck/icheck.min.js')}}"></script>
    <script>
        jQuery(document).ready(function () {
            jQuery('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });

            jQuery('#username').keyup(function(e) {
            	var content = jQuery(this).val().replace(/\s+/g, '-').toLowerCase();
            	jQuery(this).val(content);
            });
        });
    </script>
@endsection