<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PublicController@index')->name('homepage');

//  API
Route::get('api/log', 'ApiController@api_log')->name('api_log');
Route::post('card', 'ApiController@rfid_card')->name('card');
Route::get('user-info/{id}', 'ApiController@user_info')->name('info');
Route::get('logs', 'ApiController@logs')->name('logs');
Route::post('save-member/{id}', 'ApiController@save_member')->name('save_member');

Auth::routes([
	// 'register' => false
]);

// admin routes
Route::get('daily-logs', 'HomeController@daily_logs')->name('daily_logs');
Route::get('dashboard', 'HomeController@index')->name('home');
Route::get('print', 'HomeController@print_report')->name('print');

// settings
Route::get('settings', 'SettingsController@index')->name('settings');
Route::post('settings/profile', 'SettingsController@profile_update')->name('profile.update');
Route::post('settings/password', 'SettingsController@password_update')->name('password.update');

// Restful Controllers
Route::resources([
	'users' => 'UsersController',
	'roles' => 'RolesController',
	'members' => 'MembersController',
	'devices' => 'DevicesController',
]);

