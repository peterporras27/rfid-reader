@extends('admin')

@section('content')

<div class="panel panel-default panel-shadow" data-collapsed="0" style="direction: ltr;">
	
	<div class="panel-heading">
		<div class="panel-title">Recent logs</div>
		
		<div class="panel-options"></div>
	</div>
	
	<!-- panel body -->
	<div class="panel-body">
		
		<div class="row">
			
			<div class="col-md-6">
				
				<form action="{{ route('home') }}" method="GET" class="form-inline pull-left" role="form" style="direction: rtl;">

					<div class="input-group">
						@if($memberid)
						<input type="hidden" name="id" value="{{ $memberid }}">
						@endif
						<span class="input-group-btn"> 
							<button type="submit" class="btn btn-info" style="margin: 0;">GO</button>
						</span>
						<input type="number" name="show" style="width: 80px;" placeholder="10" value="{{ $perpage }}" class="form-control">
						<span class="input-group-addon">Per page</span> 
					</div>
				</form>
				
			</div>

			<div class="col-md-6">
				<form action="{{ route('print') }}" method="GET" class="form-horizontal form-groups-bordered" role="form">
				
					<input type="hidden" name="start_date" value="">
					<input type="hidden" name="end_date" value="">
					
					<div class="form-group">
						<div class="col-md-3">
							<button type="submit" class="btn btn-primary btn-md">Print Report <i class="fa fa-print"></i></button>
						</div>
						
						<div class="col-sm-5">
							<div class="daterange daterange-inline" data-format="MMMM D, YYYY" data-start-date="{{ date('F d, Y') }}" data-end-date="{{ date('F d, Y') }}">
								<i class="entypo-calendar"></i>
								<span>{{ date('F d, Y') }} - {{ date('F d, Y') }}</span>
							</div>
						</div>
						<label class="col-sm-3 control-label">
							Select Date Range:
						</label>
					</div>

				</form>
			</div>
		</div>



		<hr>
		
		@if( $logs->count() )
			
		<div class="table-responsive">
			<table class="table table-bordered table-striped table-hover">
				<thead>
					<tr>
						<th><strong>First Name</strong></th>
						<th><strong>Last Name</strong></th>
						<th><strong>Address</strong></th>
						<th><strong>Log Type</strong></th>
						<th><strong>Time</strong></th>
					</tr>
				</thead>
				<tbody>
					@foreach( $logs as $log )

						@if($log->member())
						<tr id="user-row-{{ $log->id }}">
							<td>{{ $log->member()->first_name }}</td>
							<td>{{ $log->member()->last_name }}</td>
							<td>{{ $log->member()->address }}</td>
							<td><span class="badge badge-{{ $log->log_type == 'login' ? 'success':'danger' }}">{{ $log->log_type }}</span></td>
							<td>{{ date( 'M d, Y h:i a', strtotime( $log->created_at ) ) }}</td>
						</tr>
						@endif

					@endforeach
				</tbody>
			</table>
		</div>
		{{ $logs->appends( request()->input() )->links() }}
		@else
		<div class="alert alert-info">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<strong>No records to show at the moment.</strong>
		</div>
		@endif
	</div>
</div>

<div class="modal inmodal" id="remove-user" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content animated fadeIn">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Remove Member</h4>
            </div>
            <div class="modal-body">
                <p><strong>Are you sure?</strong> once removed, this user will no longer be able to login on his/her account and all data affiliated to this account will be deleted.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="pull-right btn btn-white"  data-dismiss="modal">Cancel</button>

                <button type="button" id="remove-btn" class="btn btn-danger">Remove</button>
            </div>
        </div>
    </div>
</div>

<div id="print-id" style="">
	
</div>

@endsection

@section('action')

@endsection

@section('styles')
<!-- Toastr style -->
<link href="{{asset('css/plugins/toastr/toastr.min.css')}}" rel="stylesheet">
<link href="{{asset('assets/js/daterangepicker/daterangepicker-bs3.css')}}" rel="stylesheet">
<style>
	.daterangepicker{
		direction: ltr;
	}
</style>
@endsection

@section('scripts')

<script src="{{asset('assets/js/moment.min.js')}}"></script>
<script src="{{asset('assets/js/daterangepicker/daterangepicker.js')}}"></script>
<!-- Toastr script -->
<script src="{{asset('js/plugins/toastr/toastr.min.js')}}"></script>
<script>
jQuery(document).ready(function() {

	// Date Range Picker
	if($.isFunction($.fn.daterangepicker))
	{
		$(".daterange").each(function(i, el)
		{
			// Change the range as you desire
			var ranges = {
				'Today': [moment(), moment()],
				'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
				'Last 7 Days': [moment().subtract('days', 6), moment()],
				'Last 30 Days': [moment().subtract('days', 29), moment()],
				'This Month': [moment().startOf('month'), moment().endOf('month')],
				'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
			};

			var $this = $(el),
				opts = {
					format: attrDefault($this, 'format', 'MM/DD/YYYY'),
					timePicker: attrDefault($this, 'timePicker', false),
					timePickerIncrement: attrDefault($this, 'timePickerIncrement', false),
					separator: attrDefault($this, 'separator', ' - '),
				},
				min_date = attrDefault($this, 'minDate', ''),
				max_date = attrDefault($this, 'maxDate', ''),
				start_date = attrDefault($this, 'startDate', ''),
				end_date = attrDefault($this, 'endDate', '');

			if($this.hasClass('add-ranges'))
			{
				opts['ranges'] = ranges;
			}

			if(min_date.length)
			{
				opts['minDate'] = min_date;
			}

			if(max_date.length)
			{
				opts['maxDate'] = max_date;
			}

			if(start_date.length)
			{
				opts['startDate'] = start_date;
			}

			if(end_date.length)
			{
				opts['endDate'] = end_date;
			}


			$this.daterangepicker(opts, function(start, end)
			{
				var drp = $this.data('daterangepicker');

				if($this.is('[data-callback]'))
				{
					//daterange_callback(start, end);
					callback_test(start, end);
				}

				if($this.hasClass('daterange-inline'))
				{
					$this.find('span').html(start.format(drp.format) + drp.separator + end.format(drp.format));
				}
			});
		});

		$('.daterange').on('apply.daterangepicker', function(ev, picker) {
			$('[name="start_date"]').val( picker.startDate.format('MM/DD/YYYY') );
			$('[name="end_date"]').val( picker.endDate.format('MM/DD/YYYY') );
		});
	}

	toastr.options = {
		"closeButton": true,
		"debug": false,
		"progressBar": true,
		"positionClass": "toast-top-right",
		"onclick": null,
		"showDuration": "400",
		"hideDuration": "1000",
		"timeOut": "7000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
	}

	jQuery('button[data-name="delete"]').click(function(e) {
		e.preventDefault();
		var token = jQuery('meta[name="csrf-token"]').attr('content');
	});

	jQuery('#remove-user').on('show.bs.modal', function (event) {
		var button = jQuery(event.relatedTarget); // Button that triggered the modal
		var user_id = button.data('id');
		jQuery('#remove-btn').data('id',user_id);
  		jQuery(this).find('.modal-title').text('Remove ' + jQuery('#user-row-'+user_id+' td:first').html() );
	});

	jQuery('#user-qrcode').on('show.bs.modal', function (event) {

		var button = jQuery(event.relatedTarget); // Button that triggered the modal
		var hash = button.data('hash');
		var card = button.data('card');

		jQuery('#qrcode-img').attr('src', '{{ url('/') }}/qr/250/png/'+hash);

		var data = button.data('json');
		var html = '<b>ID:</b> '+data.card_number+'<br>';
		html += '<b>First Name:</b> '+data.first_name+'<br>';
		html += '<b>Middle Name:</b> '+data.middle_name+'<br>';
		html += '<b>Last Name:</b> '+data.last_name+'<br>';
		html += '<b>Gender:</b> '+data.gender+'<br>';
		html += '<b>Birth Day:</b> '+data.birthdate+'<br>';
		html += '<b>Address:</b> '+data.address+'<br>';
		html += '<b>Phone Number:</b> '+data.phone_number+'<br>';

  		jQuery(this).find('#user-info').html( html );
  		jQuery('#card-link').attr('href',card);

	});

	jQuery('#remove-btn').click(function(e) {
		e.preventDefault();
		var user_id = jQuery(this).data('id');
		var token = jQuery('meta[name="csrf-token"]').attr('content');

		jQuery.ajax({  
			url: '{{ route('members.index') }}/'+user_id,
			type: 'POST',
			dataType: 'json',
			data: { _token: token, _method: 'DELETE' },
		})
		.always(function(data) {
			if (data.error) {
				toastr.error(data.message,'Error');
			} else {
				jQuery('#remove-user').modal('toggle');
				toastr.success(data.message,'Success');
				jQuery('#user-row-'+user_id).remove();
			}
		});
		 
	});
});
</script>
@endsection
