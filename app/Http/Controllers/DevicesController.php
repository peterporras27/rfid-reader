<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Devices;
use Validator;

class DevicesController extends Controller
{
    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->params = array(
            'title' => 'Devices (RFID Scanners)',
            'description' => 'Manage RFID Scanner devices.',
        );

        $this->validate = array(
            'name' => 'required|string|max:255',
            'ip_address' => 'required|string|max:255',
            'type' => 'required|string|max:255'
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request )
    {
        $search = ( $request->input('search') ) ? $request->input('search'): '';
        $perpage = preg_replace('/\D/', '', $request->input('show') );
        $show = ( empty( $perpage ) || $perpage == 0 ) ? 10: $perpage;
        $devices = Devices::orderBy('created_at','asc');
        $where = [];

        if ( $search ) {
            $where[] = ['name', 'LIKE', "%$search%"];
            $devices->where( $where );
            $devices->orWhere('ip_address', 'LIKE', "%$search%");
        }

        if ( count($where) > 0 ) {
            $devices->where( $where );
        }

        $this->params['search'] = $search;
        $this->params['perpage'] = $show;
        $this->params['devices'] = $devices->paginate( $show );
        $this->params['type'] = array(
            'inactive' => 'Inactive device',
            'login' => 'For login',
            'logout' => 'For logout',
            'assign' => 'For assigning RFID to members'
        );

        return view('devices.index', $this->params);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('devices.create', $this->params);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Block devices who are not admin
        $request->user()->authorizeRoles('admin');

        $validator = Validator::make( $request->all(), $this->validate );

        if ( $validator->fails() ) {
            return redirect('devices/create')
                ->withErrors( $validator )
                ->withInput();
        }

        $device = new Devices();
        $device->fill( $request->all() );
        $device->save();

        return redirect('devices/'.$device->id.'/edit')->with('success', 'Device ' . $device->name . ' successfuly added.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Devices $device)
    {
        $this->params['title'] = 'Edit RFID Scanner Details';
        $this->params['device'] = $device;

        return view('devices.edit', $this->params);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Block members who are not admin
        $request->user()->authorizeRoles('admin');

        $device = Devices::find( $id );

        // double check if device exist.
        if ( ! $device ) {
            return redirect('devices')->with('error', 'device does not exist, please try again.');
        }

        $validator = Validator::make( $request->all(), $this->validate );

        if ( $validator->fails() ) {
            return redirect('devices/'. $device->id .'/edit')
                ->withErrors( $validator )
                ->withInput();
        }

        $device->fill( $request->all() );
        $device->save();

        return redirect('devices/'. $device->id .'/edit')->with('success', 'Device details successfuly updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        // Block members who are not admin
        $request->user()->authorizeRoles('admin');

        $device = Devices::find( $id );

        if ( ! $device ) {
            return response()->json([
                'error' => true,
                'message' => 'Please try again.'
            ]);
        } 

        $device->delete();

        return response()->json([
            'error' => false,
            'message' => 'Device successfuly removed.'
        ]);
    }
}
