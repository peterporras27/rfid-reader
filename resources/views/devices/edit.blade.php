@extends('admin')

@section('content')

<div class="panel panel-default panel-shadow" data-collapsed="0" style="direction: ltr;">
	
	<div class="panel-heading">
		<div class="panel-title">Edit RFID Scanner Device</div>
		
		<div class="panel-options"></div>
	</div>
	
	<!-- panel body -->
	<div class="panel-body">
		
		<form action="{{ route('devices.update', $device->id) }}" method="POST" enctype="multipart/form-data" role="form">
			{{ csrf_field() }}
			<input type="hidden" name="_method" value="PUT">
			<div class="row">

				<div class="col-md-6">

					<div class="form-group">
						<label for="type">RFID Scanner Usage</label>
						<select name="type" class="form-control" id="type">
							<option value="inactive"{{ $device->type == 'inactive' ? ' selected':'' }}>Inactive device</option>
							<option value="login"{{ $device->type == 'login' ? ' selected':'' }}>For login</option>
							<option value="logout"{{ $device->type == 'logout' ? ' selected':'' }}>For logout</option>
							<option value="assign"{{ $device->type == 'assign' ? ' selected':'' }}>For assigning RFID to members</option>
						</select>
					</div>

				</div>

				<div class="col-md-6">
					
					<div class="form-group">
						<label for="name">Device Name</label>
						<input type="text" value="{{ $device->name }}" name="name" class="form-control" id="name" placeholder="Device Name">
					</div>

					<div class="form-group">
						<label for="ip_address">IP Address</label>
						<input type="text" value="{{ $device->ip_address }}" name="ip_address" class="form-control" id="ip_address" placeholder="0.0.0.0">
					</div>
					
				</div>

			</div>
			<hr>
			<button type="submit" class="btn btn-primary btn-icon">Save <i class="fa fa-save"></i></button>
		</form>
		
	</div>
</div>

@endsection

@section('action')
<div class="title-action">
<a href="{{ route('devices.index') }}" class="btn btn-default btn-icon icon-left">
	Go back <i class="fa fa-chevron-left"></i> 
</a>
</div>
@endsection

@section('styles')

@endsection

@section('scripts')

@endsection