<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MemberLog;
use App\Member;
use Validator;
use Storage;
use Image;

class MembersController extends Controller
{
    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->params = array(
            'title' => 'Members',
            'description' => 'Manage all members.',
        );

        $this->validate = array(
            'photo' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'middle_name' => 'required|string|max:255',
            'address' => 'required|string|max:255',
            'phone_number' => 'required|string',
            'active' => 'boolean',
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request )
    {

        $search = ( $request->input('search') ) ? $request->input('search'): '';
        $perpage = preg_replace('/\D/', '', $request->input('show') );
        $show = ( empty( $perpage ) || $perpage == 0 ) ? 10: $perpage;

        $members = Member::orderBy('first_name','asc');

        $where = [];
        
        if ( $search ) { 

            $where[] = ['first_name', 'LIKE', "%$search%"];

            $members->where( $where );
            $members->orWhere('last_name', 'LIKE', "%$search%");
            $members->orWhere('middle_name', 'LIKE', "%$search%");
            
        } else {

            $members->where( $where );
        }

        $this->params['search']     = $search;
        $this->params['perpage']    = $show;
        $this->params['members']   = $members->paginate( $show );

        return view('members.index', $this->params);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        // Block members who are not admin
        $request->user()->authorizeRoles('admin');
        
        $this->params['title'] = 'Register new member';
        $this->params['description'] = 'Customize and register new member account.';

        return view('members.create', $this->params);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make( $request->all(), $this->validate );

        if ( $validator->fails() ) {
            return redirect('members/create')
                ->withErrors( $validator )
                ->withInput();
        }

        $member = new Member();
        $member->fill( $request->all() );
        if ( $request->hasFile('photo') ) {

            if ( !$request->file('photo')->isValid() ) {
                return redirect('members/create')->withInput()->with('error', 'The photo you uploaded is invalid.');
            }

            $fname =  str_replace(' ', '_', $request->input('first_name').'_'.$request->input('last_name') );
            $directory = public_path() . '/uploads/';
            $filename = strtolower( $fname ) . '.' . $request->photo->extension();
            $upload_path = $directory . $filename;
            Image::make( $request->photo )->save( $upload_path );

            $member->photo = $filename;
        }

        // $member->card_number = $this->randomNumber(15);
        $member->save();

        return redirect('members/'.$member->id.'/edit')->with('success', 'Member ' . $member->first_name . ' successfuly added.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $this->params['title'] = 'Edit Member Info';
        $this->params['description'] = 'Update member information.';

        $member = Member::find( $id );

        if ( ! $member ) {
            return redirect('members')->with('warning', 'Member no longer exist.');
        }

        $this->params['member'] = $member;
        $this->params['photo'] = 'uploads/'.$member->photo;

        return view('members.edit', $this->params);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update( Request $request, $id )
    {

        $member = Member::find( $id );

        // double check if member exist.
        if ( ! $member ) {
            return redirect('members')->with('error', 'member does not exist, please try again.');
        }

        $validator = Validator::make( $request->all(), $this->validate );

        if ( $validator->fails() ) {
            return redirect('members/'. $member->id .'/edit')
                ->withErrors( $validator )
                ->withInput();
        }

        if ( $request->hasFile('photo') ) {

            if ( !$request->file('photo')->isValid() ) {
                return redirect('members/create')->withInput()->with('error', 'The photo you uploaded is invalid.');
            }

            $fname =  str_replace(' ', '_', $request->input('first_name').'_'.$request->input('last_name') );
            $directory = public_path() . '/uploads/';
            $filename = strtolower( $fname ) . '.' . $request->photo->extension();
            $upload_path = $directory . $filename;

            Image::make( $request->photo )->save( $upload_path );

            $member->photo = $filename;
        }

        $member->fill( $request->all() );

        if ( $request->input('active') ) {

            $member->active = true;
            $member->scan = ( is_null($member->card_number) || empty($member->card_number) ) ? true:false;

        } else {

            $member->scan = false;
            $member->active = false;
            $member->card_number = '';
        }

        $member->save();

        return redirect('members/'. $member->id .'/edit')->with('success', 'Member details successfuly updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy( Request $request, $id )
    {
        // Block members who are not admin
        $request->user()->authorizeRoles('admin');

        $member = Member::find( $id );

        if ( ! $member ) {
            return response()->json([
                'error' => true,
                'message' => 'Please try again.'
            ]);
        } 

        if ($member->photo) {
            unlink(public_path() . '/uploads/'.$member->photo);   
        }

        $member->delete();

        return response()->json([
            'error' => false,
            'message' => 'Member successfuly removed.'
        ]);
    }

    public function randomNumber($length) {
        $result = '';

        for($i = 0; $i < $length; $i++) {
            $result .= mt_rand(0, 9);
        }

        return $result;
    }
}
