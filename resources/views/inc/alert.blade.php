<div class="col-md-12">

    @foreach ( $errors->all() as $message )
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <strong>Error:</strong> {{ $message }}
        </div>
    @endforeach

    @if ( isset( $success_message ) && $success_message )
        <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <strong>Success:</strong> {{ $success_message }}
        </div>
    @endif

    @if ( isset( $error_message ) && $success_message )
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <strong>Error:</strong> {{ $error_message }}
        </div>
    @endif

    @if ( session('success') )
        <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <strong>Success:</strong> {{ session('success') }}
        </div>
    @endif

    @if ( session('error') )
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <strong>Error:</strong> {{ session('error') }}
        </div>
    @endif

    @if ( session('warning') )
        <div class="alert alert-warning">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <strong>Warning:</strong> {{ session('warning') }}
        </div>
    @endif

</div>