@extends('layouts.public')

@section('content')

<div class="middle-box text-center loginscreen  animated fadeInDown">
    <div>
        <div>
            <h1 class="logo-name" style="font-size: 60px;">RESET PASSWORD</h1>
        </div>
        <form class="m-t" method="POST" role="form" action="{{ route('password.email') }}">
            {{ csrf_field() }}
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <input name="email" value="{{ old('email') }}" type="email" class="form-control" placeholder="Email" required="">
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>

            <button type="submit" class="btn btn-primary block full-width m-b">Send Password Reset Link</button>

            <a href="{{ route('login') }}"><small>login</small></a>
            {{-- <p class="text-muted text-center"><small>Do not have an account?</small></p>
            <a class="btn btn-sm btn-white btn-block" href="register.html">Create an account</a> --}}
        </form>
        <p class="m-t"> <small>&copy; {{ date('Y') }}</small> </p>
    </div>
</div> 

@endsection
