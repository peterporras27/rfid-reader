<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MemberLog extends Model
{
	protected $fillable = [
        'purpose',
        'log_type',
        'member_id',
    ];
    
    public function member()
    {
      	return $this->hasOne(
            'App\Member', 
            'id', // foreign key
            'member_id' // local key
        )->first();
    }
}
