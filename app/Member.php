<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    protected $fillable = [
        'first_name',
        'last_name',
        'middle_name',
        'address',
        'user_id',
        'phone_number',
        'card_number',
        'active',
    ];

    public function logs()
    {
      
        return $this->hasMany(
            'App\MemberLog', 
            'member_id', // foreign key
            'id' // local key
        )->first();
    }
}
