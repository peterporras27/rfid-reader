<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PublicController extends Controller
{
    public function index()
    {
        if (auth()->check()) {

            if (auth()->user()->hasRole('admin')) {
                return redirect()->route('daily_logs');
            } else {
                return redirect('/dashboard?id='.auth()->user()->id);
            }
        }

        return view('welcome'); 
    }
}
