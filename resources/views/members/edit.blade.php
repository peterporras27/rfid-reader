@extends('admin')

@section('content')

<div class="panel panel-default panel-shadow" data-collapsed="0" style="direction: ltr;">
	
	<div class="panel-heading">
		<div class="panel-title">Add member</div>
		
		<div class="panel-options"></div>
	</div>
	
	<!-- panel body -->
	<div class="panel-body">
		
		<form action="{{ route('members.update', $member->id) }}" method="POST" enctype="multipart/form-data" role="form">
			{{ csrf_field() }}
			<input type="hidden" name="_method" value="PUT">
			<div class="row">
				
				<div class="col-md-6">
					
					@if($member->photo)
					<img src="{{ asset('uploads/'.$member->photo) }}" class="img-fluid img-thumbnail">
					@endif
					<div class="form-group">
						<label for="photo">Photo</label>
						<input type="file" name="photo" class="form-control" id="photo" placeholder="Upload Photo">
					</div>

					<div class="form-group">
						<label for="address">Address</label>
						<input type="text" value="{{ $member->address }}" name="address" class="form-control" id="address" placeholder="Address">
					</div>

					<div class="form-group">
						<label for="phone_number">Phone Number</label>
						<input type="text" value="{{ $member->phone_number }}" name="phone_number" class="form-control" id="phone_number" placeholder="Phone Number">
					</div>

					<div class="form-group">
						<label for="name">Activate Member</label><br>
						<input type="checkbox" name="active" value="1" class="js-switch" {{ $member->active ? 'checked':'' }} />
						<span id="active-status">Active</span>
					</div>
					
				</div>

				<div class="col-md-6">

					<div class="form-group">
						<label for="first_name">First Name</label>
						<input type="text" value="{{ $member->first_name }}" name="first_name" class="form-control" id="first_name" placeholder="First name">
					</div>

					<div class="form-group">
						<label for="last_name">Last Name</label>
						<input type="text" value="{{ $member->last_name }}" name="last_name" class="form-control" id="last_name" placeholder="Last name">
					</div>

					<div class="form-group">
						<label for="middle_name">Middle Name</label>
						<input type="text" value="{{ $member->middle_name }}" name="middle_name" class="form-control" id="middle_name" placeholder="Middle name">
					</div>

					<div class="form-group">
						<label for="card_number">RFID</label>
						<input type="text" value="{{ $member->card_number }}" class="form-control" disabled>
					</div>

				</div>
			</div>
			<hr>
			<button type="submit" class="btn btn-primary btn-lg btn-icon">Save <i class="fa fa-save"></i></button>
		</form>
		
	</div>
</div>

@endsection

@section('action')
<div class="title-action">
	@if ( Auth::user()->hasRole('admin') )
	<a href="{{ route('members.index') }}" class="btn btn-default btn-icon icon-left">
		Go back <i class="fa fa-chevron-left"></i> 
	</a>
	@endif
</div>
@endsection

@section('styles')
<link href="{{asset('css/plugins/iCheck/custom.css')}}" rel="stylesheet">
<link rel="stylesheet" href="{{asset('assets/js/daterangepicker/daterangepicker-bs3.css')}}">
<link href="{{asset('css/plugins/switchery/switchery.css')}}" rel="stylesheet">
<style>.datepicker{right:auto;}</style>
@endsection

@section('scripts')
<script src="{{asset('js/plugins/datapicker/bootstrap-datepicker.js')}}"></script>
<script src="{{asset('js/plugins/switchery/switchery.js')}}"></script>
<script src="{{asset('js/plugins/toastr/toastr.min.js')}}"></script>
<script>
var condition = false;
var timer = null;

jQuery(document).ready(function($) {

	toastr.options = {
		"closeButton": true,
		"debug": false,
		"progressBar": true,
		"positionClass": "toast-top-right",
		"onclick": null,
		"showDuration": "400",
		"hideDuration": "1000",
		"timeOut": "7000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
	}

	$('#birthdate, #renewal_date').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true
    });
    var switchBtn = document.querySelector('.js-switch');
    var switchButton = new Switchery(switchBtn, { color: '#1AB394' });
    switchBtn.onchange = function() {
    	if ( switchBtn.checked ) {
    		$('.js-switch').attr('checked','checked');
    		$('#active-status').text('Active');
    		$('.rfid-inputs').fadeIn();
    	} else {
    		$('.js-switch').removeAttr('checked');
    		$('#active-status').text('Inactive');
    		$('.rfid-inputs').fadeOut();
    	}
	};

	$('#scan').click(function (e) {

		e.preventDefault();
		var d = 'Scan new card';
		$(this).prop('disabled',true);
		$(this).addClass('disabled');
		$(this).html('Waiting for card to be scanned...');

		timer = setInterval(myFunction, 2000);
	});

});

function myFunction() {

	console.log(condition);

	if(condition) {
		clearInterval(timer);
		return;
	}

	//do stuff
	jQuery.ajax({
		url: '{{ route('info',$member->id) }}',
		type: 'get',
		data: {},
		success: function (data) {
			
			if (data.error) {
				
				condition = false;
				// $('#scan').prop('disabled',false);
				// $('#scan').removeClass('disabled');
				// $('#scan').html('Scan new card');
				// $('#card_number').val('');
				// toastr.error(data.message,'Error');

			} else {

				condition = true;
				$('#scan').prop('disabled',false);
				$('#scan').removeClass('disabled');
				$('#scan').html('Scan new card');
				toastr.success(data.message,'Success');
				if (data.data.card_number) {
					$('#card_number').val(data.data.card_number);
				} else {
					$('#card_number').val('');
				}
			}
		}
	});
}
</script>
@endsection