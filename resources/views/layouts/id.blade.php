
<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>{{ $member->first_name }}</title>
        <style>
            @media print {
                .banner {
                    background-color: #2076a5 !important;
                    -webkit-print-color-adjust: exact; 
                    color: white !important;
                    font-family: sans-serif;
                }
            }
            .idbox .underline {
                border-bottom: 1px solid #2076a5;
                font-size: 18px;
                color:  #2076a5 !important;
            }
            .idbox .date{
                width:13%;
                padding:15px;
                border: 1px solid #2076a5;
                float:left;
                color:  #2076a5 !important;
                margin-bottom: 23px;
            }

            .idbox h5{margin:0;}
            .idbox strong, .idbox p, .idbox i{color:  #2076a5 !important;}
            
        </style>
    </head>
    <body>
        
        <div class="idbox" style="float:left;width: 300px;border: 1px solid #ccc;margin-right:15px;">

            <div class="banner" align="center" style="padding: 15px;background: #2076a5;color:#fff;font-family: sans-serif;">
                <h4 style="margin: 0;">TOMAS CONFESOR MEMORIAL<BR>PUBLIC LIBRARY</h4>
                <h5 style="margin: 0;">San Agustin St., Cabatuan, Iloilo</h5>
            </div>
        
            <div style="padding: 15px;">

                <div style="float:left;width:49%;" align="center">
                    @if ( $member->photo ) 
                    <img src="{{ url('/') }}/uploads/{{ $member->photo }}" style="width:100%;">
                    @else
                    <div style="border: 1px solid #ccc;width: 132px;height: 132px;"></div>
                    @endif
                </div>

                <div style="float:left; width: 49%;" align="center">
                    <img src="{{ url('/') }}/qr/250/png/{{ $member->card_number }}" style="width:100%;">
                </div>
                <div style="clear:both;"></div>
                <div style="width:100%;margin-top: 0px;line-height: 1.8;font-size:13px;" align="center">
                    <div class="underline">
                        {{ $member->card_number }}
                    </div>
                    <strong>ID No.</strong>
                    <div class="underline">
                        {{ $member->first_name . ' ' . $member->middle_name . ' '. $member->last_name }}<br>
                    </div>
                    <strong>Name</strong>
                    <div class="underline">
                        {{ $member->address }}
                    </div>
                    <strong>Home Address</strong>
                    <div class="underline">
                        {{ $member->phone_number }}<br>
                    </div>
                    <strong>Contact No.</strong> 
                </div>
            </div>
            
        </div>


        <div class="idbox" style="float:left;width: 300px;border: 1px solid #ccc;">

            <div class="banner" align="center" style="padding: 15px;background: #2076a5;color:#fff;font-family: sans-serif;">
                <h4 style="margin: 0;">IMPORTANT</h4>
            </div>
        
            <div style="padding: 15px;">
                <div style="width:100%;margin-top: 0px;line-height: 1.8;font-size:13px;" align="center">
                    <p>This Card is Personal Card, and the owner cannot lend it to anybody under penalty of losing his library privvilages.</p>
                    <p>The Owner is responsible for all the books issued under the name in serial number appearing on the face of this card.</p>
                    <p>Any tampering or defacing of this will render it null and void.</p>
                </div>
                <div class="date">{{ date('Y') }}</div>
                <div class="date">{{ date('Y') + 1 }}</div>
                <div class="date">{{ date('Y') + 2 }}</div>
                <div class="date">{{ date('Y') + 3 }}</div>
                <div style="clear:both;"></div>
                <div align="center">
                    <h5 style="color:  #2076a5 !important;">ELIADORA A. PEDROSO</h5>
                    <div><i>Computer File Librarian II</i></div>
                    <br>
                    <h5 style="color:  #2076a5 !important;">EDUARDO S. TUARES</h5>
                    <div><i>Municipal Vide Mayor</i></div>
                </div>
            </div>
            
        </div>
        <div style="clear:both;"></div>
        <script>window.print();</script>
    </body>
</html>
