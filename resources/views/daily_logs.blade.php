<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>RFID READER</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="{{asset('bootstrap/css/bootstrap.min.css')}}">

        <style>
        .wrap {
            background: url(../../img/bg-dark.jpg) no-repeat center center;
            background-size: cover;
            min-height: 900px;
        }
        tbody{
            width: 100%;
        }
        </style>
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="#">RFID Scanner</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    @if (Route::has('login'))
                        @if (Auth::check())
                            @if ( Auth::user()->hasRole('admin') )
                                <li class="nav-item active">
                                    <a class="nav-link" href="{{ route('home') }}">Dashboard</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('members.index') }}">Members</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('users.index') }}">Users</a>
                                </li>
                            @endif
                            @if ( Auth::user()->hasRole('member') )
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('members.edit', Auth::user()->member()) }}">Account Details</a>
                            </li>
                            @endif
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('settings') }}">Settings</a>
                            </li>
                        @else
                            <li class="nav-item active">
                                <a class="nav-link" href="{{ route('login') }}">Login</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('register') }}">Register</a>
                            </li>
                        @endif
                    @endif
                </ul>
            </div>
        </nav>
        
        <div class="wrap">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-3">
                    <div align="center">
                        <img src="{{ asset('img/logo.png') }}" alt="" class="img-fluid mt-5">
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-md-9">
                    
                    <div class="card bg-light border-light mt-5 mb-5">
                        <div class="card-header">Scan Details</div>
                        <div class="card-body text-dark">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="photo" align="center">
                                        <img width="100%" src="{{ asset('img/placeholder.jpg') }}" alt="" class="img-fluid img-thumbnail">
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <table class="table table-inverse">
                                        <tbody>
                                            <tr>
                                                <td><b>Status:</b></td>
                                                <td>
                                                    <span class="badge badge-info mt-1">STATUS</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><b>Date & Time:</b></td>
                                                <td class="time"></td>
                                            </tr>
                                            <tr>
                                                <td><b>First Name:</b></td>
                                                <td class="fname"></td>
                                            </tr>
                                            <tr>
                                                <td><b>Last Name:</b></td>
                                                <td class="lname"></td>
                                            </tr>
                                            <tr>
                                                <td><b>Middle Name:</b></td>
                                                <td class="mname"></td>
                                            </tr>
                                            {{-- <tr>
                                                <td><b>Address:</b></td>
                                                <td class="addr"></td>
                                            </tr>
                                            <tr>
                                                <td><b>Phone:</b></td>
                                                <td class="phone"></td>
                                            </tr> --}}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
        
        <script src="{{ asset('js/app.js') }}"></script>
        <script>
        jQuery(document).ready(function($) {
            setInterval(function(){
                jQuery.ajax({
                    url: '{{ route('logs') }}',
                    type: 'get',
                    data: {},
                    success: function (data) {
                        $('.fname').text(data.first_name);
                        $('.lname').text(data.last_name);
                        $('.mname').text(data.middle_name);
                        // $('.addr').text(data.address);
                        // $('.phone').text(data.phone);
                        if (data.photo != '') {
                            $('.photo img').attr('src',data.photo);
                        } else {
                            $('.photo img').attr('src','img/placeholder.jpg');
                        }
                        var btn = $('.table .badge');
                        switch(data.log_type){
                            case '':
                                btn.attr('class','badge badge-info mt-1');
                                btn.html('STATUS');
                                $('.table .time').html('');
                            break;
                            case 'logout':
                                btn.attr('class','badge badge-danger mt-1');
                                btn.html('LOGGED OUT');
                                $('.table .time').html(data.time);
                            break;
                            case 'login':
                                btn.attr('class','badge badge-success mt-1');
                                btn.html('LOGGED IN');
                                $('.table .time').html(data.time);
                            break;
                        }
                    }
                });
            },1000);
        });
        </script>
    </body>
</html>
