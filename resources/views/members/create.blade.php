@extends('admin')

@section('content')

<div class="panel panel-default panel-shadow" data-collapsed="0" style="direction: ltr;">
	
	<div class="panel-heading">
		<div class="panel-title">Add member</div>
		
		<div class="panel-options"></div>
	</div>
	
	<!-- panel body -->
	<div class="panel-body">
		
		<form action="{{ route('members.store') }}" method="POST" enctype="multipart/form-data" role="form">
			{{ csrf_field() }}

			<div class="row">
				
				<div class="col-md-6">
					
					<div class="form-group">
						<label for="photo">Photo</label>
						<input type="file" name="photo" class="form-control" id="photo" placeholder="Upload Photo">
					</div>

					<div class="form-group">
						<label for="address">Address</label>
						<input type="text" value="{{ old('address') }}" name="address" class="form-control" id="address" placeholder="Address">
					</div>

					<div class="form-group">
						<label for="phone_number">Phone Number</label>
						<input type="text" value="{{ old('phone_number') }}" name="phone_number" class="form-control" id="phone_number" placeholder="Phone Number">
					</div>

					
					<div class="form-group">
						<label for="name">Activate Member</label><br>
						<input type="checkbox" name="active" value="1" class="js-switch" {{ old('active') ? 'checked':'' }} />
						<span id="active-status">Active</span>
					</div>
					
				</div>

				<div class="col-md-6">

					<div class="form-group">
						<label for="first_name">First Name</label>
						<input type="text" value="{{ old('first_name') }}" name="first_name" class="form-control" id="first_name" placeholder="First name">
					</div>

					<div class="form-group">
						<label for="last_name">Last Name</label>
						<input type="text" value="{{ old('last_name') }}" name="last_name" class="form-control" id="last_name" placeholder="Last name">
					</div>

					<div class="form-group">
						<label for="middle_name">Middle Name</label>
						<input type="text" value="{{ old('middle_name') }}" name="middle_name" class="form-control" id="middle_name" placeholder="Middle name">
					</div>

				</div>
			</div>
			<hr>
			<button type="submit" class="btn btn-primary btn-icon">Register <i class="fa fa-save"></i></button>
		</form>
		
	</div>
</div>

@endsection

@section('action')
<div class="title-action">
	@if ( Auth::user()->hasRole('admin') )
		<a href="{{ route('members.index') }}" class="btn btn-default btn-icon icon-left">
			Go back <i class="fa fa-chevron-left"></i> 
		</a>
	@endif
</div>
@endsection

@section('styles')
<link href="{{asset('css/plugins/iCheck/custom.css')}}" rel="stylesheet">
<link rel="stylesheet" href="{{asset('assets/js/daterangepicker/daterangepicker-bs3.css')}}">
<link href="{{asset('css/plugins/switchery/switchery.css')}}" rel="stylesheet">
<style>.datepicker{right:auto;}</style>
@endsection

@section('scripts')
<script src="{{asset('js/plugins/datapicker/bootstrap-datepicker.js')}}"></script>
<script src="{{asset('js/plugins/switchery/switchery.js')}}"></script>
<script>
	jQuery(document).ready(function($) {
		$('#birthdate, #renewal_date').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true
        });
        var switchBtn = document.querySelector('.js-switch');
        var switchButton = new Switchery(switchBtn, { color: '#1AB394' });
        switchBtn.onchange = function() {
        	if ( switchBtn.checked ) {
        		$('.js-switch').attr('checked','checked');
        		$('#active-status').text('Active');
        	} else {
        		$('.js-switch').removeAttr('checked');
        		$('#active-status').text('Inactive');
        	}
		};
	});
</script>
@endsection