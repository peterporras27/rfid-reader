<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$role_admin = Role::where('name', 'admin')->first();

    	$admin = new User();
        $admin->name = 'Administrator';
        $admin->first_name = 'Admin';
    	$admin->last_name = '';
    	$admin->email = 'admin@admin.com';
    	$admin->password = bcrypt('secret');
    	$admin->save();

    	$admin->roles()->attach($role_admin);
    }
}
