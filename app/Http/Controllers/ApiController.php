<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Member;
use App\MemberLog;
use App\Devices;
use QrCode;
use Carbon\Carbon;

class ApiController extends Controller
{

    public function save_member(Request $request, $id)
    {
        $member = Member::find( $id );

        $response = array(
            'error' => true,
            'message' => 'Please try again.',
            'data' => '',
        );

        // double check if member exist.
        if ( ! $member ) {
            $response['message'] = 'member does not exist, please try again.';
            return response()->json( $response );
        }

        $member->scan = true;
        $member->active = false;
        $member->card_number = '';
        $member->save();

        $response['error'] = false;
        $response['message'] = 'scan: TRUE, active: FALSE, card_number: EMPTY';
        return response()->json( $response );
    }
   
    public function rfid_card(Request $request)
    {
        $response = array(
            'error' => false,
            'message' => 'Please try again.',
            'data' => '',
        );

        if ( !$request->input('ip') ) {
            return 'Please try again.';
        }

        $device = Devices::where('ip_address','=',$request->input('ip'))->first();

        if ( !$device ) 
        {
            $device = new Devices;
            $device->name = 'New Device';
            $device->ip_address = $request->input('ip');
            $device->save();

            return 'Device successfully added!';
        }

        $message = 'Kindly activate device.';

        switch($device->type)
        {
            case 'login':

                $member = Member::where('card_number','=',$request->input('rfid'))->first();

                if ($member) {

                    $log = new MemberLog();
                    $log->log_type = 'login'; // login, logout
                    // $log->purpose = '';
                    $log->member_id = $member->id;
                    $log->save();

                    $message = $member->first_name.' logged out successfully.';

                    $sms = "Good Day Dear Parent! Your child name: ".$member->first_name." ".$member->last_name." ";
                    $sms .= "has entered the school premises on date: ".date('d/m/Y')." at time: ".date('H:m:s a').". ";
                    $sms .= "Have a nice day ahead of you. Thank you.";

                    $this->sendSMS(
                        $member->phone_number, 
                        $sms
                    );
                    
                } else {

                    $message = 'Card not registered.';
                }

            break;
            case 'logout':

                $member = Member::where('card_number','=',$request->input('rfid'))->first();

                if ($member) {

                    $log = new MemberLog();
                    $log->log_type = 'logout'; // login, logout
                    // $log->purpose = '';
                    $log->member_id = $member->id;
                    $log->save();

                    $message = $member->first_name.' logged out successfully.';

                    $sms = "Good Day Dear Parent! Your child name: ".$member->first_name." ".$member->last_name." ";
                    $sms .= "has left the school premises on date: ".date('d/m/Y')." at time: ".date('H:m:s a').". ";
                    $sms .= "Have a nice day ahead of you. Thank you.";

                    $this->sendSMS(
                        $member->phone_number, 
                        $sms
                    );
                    
                } else {

                    $message = 'Card not registered.';
                }

            break;
            case 'assign':

                $member = Member::where([
                    ['scan','=',true],
                    ['active','=',false],
                    ['card_number','=','']
                ])->latest('updated_at')->first();

                if ( !$member ) {
                    return $message;
                }

                $member->scan = false;
                $member->active = true;
                $member->card_number = $request->input('rfid');
                $member->save();

            break;
        }

        return $message;
    }

    public function user_info($id)
    {
        $member = Member::find($id);
        $response = array(
            'error' => true,
            'message' => '',
            'data' => '',
        );

        if ($member) {

            if ( $member->scan == false && $member->active == true ) 
            {
                $response['error'] = false;
                $response['message'] = 'RFID successfully assigned.';
                $response['data'] = $member;
            }
        }

        return response()->json( $response );
    }

    public function api_log( Request $request )
    {

        $response = array(
            'error' => false,
            'message' => '',
            'data' => '',
        );

        // Check if event key exist
        if ( !$request->input('card_number') ) {
            $response['error'] = true;
            $response['message'] = 'ERROR: member not found.';
            return response()->json( $response );
        }

        // Find user who owns specific hash code.
        $member = Member::where( 'card_number', '=', $request->input('card_number') )->first();

        // Check if member exist.
        if ( ! $member ) {
            $response['error'] = true;
            $response['message'] = 'ERROR: member not found.';
            return response()->json( $response );
        }

        // Check if login type
        if ( !$request->input('type') ) {
            $response['error'] = true;
            $response['message'] = 'ERROR: Select login type.';
            return response()->json( $response );
        }

        $logTypes = array('login','logout');
        if ( !in_array( $request->input('type'), $logTypes ) ) {
            $response['error'] = true;
            $response['message'] = 'ERROR: Select login type.';
            return response()->json( $response );
        }

        $logtype = strtolower( $request->input('type') );

        $name = $member->first_name . ' ' . $member->last_name;

        $log = new MemberLog();
        $log->log_type = $logtype; // login, logout, onetime
        $log->purpose = 'Research';
        $log->member_id = $member->id;
        $log->save();

        $response['message'] = 'SUCCESS: '.$name . ' log recorded!';
        

        return response()->json( $response );
    }

    public function logs(){

        $return = [
            'first_name' => '',
            'last_name' => '',
            'middle_name' => '',
            'address' => '',
            'phone' => '',
            'photo' => '',
            'log_type' => '',
            'time' => ''
        ];

        $log = MemberLog::latest()->first();

        if ($log) {

            $member = $log->member();
            
            $return = [
                'first_name' => $member->first_name,
                'last_name' => $member->last_name,
                'middle_name' => $member->middle_name,
                'address' => $member->address,
                'phone' => $member->phone_number,
                'photo' => asset('uploads/'.$member->photo),
                'log_type' => $log->log_type,
                'time' => $log->created_at->format('F m, Y H:ia')
            ];
        }

        return response()->json( $return );
    }


    public function sendSMS($phone,$message)
    {
        $args = [
          'phone' => $phone,
          'message' => $message,
          'code' => 'rfid'
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,"https://sms.rfid.tk");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($args) );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close ($ch);

        // return $response;
    }
}