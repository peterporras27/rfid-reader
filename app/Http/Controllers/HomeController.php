<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MemberLog;
use App\Member;
use Validator;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->params = array(
            'title' => 'DAILY LOGS',
            'description' => 'List of daily logs',
        );
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request )
    {
        $perpage = preg_replace('/\D/', '', $request->input('show') );
        $memberid = preg_replace('/\D/', '', $request->input('id') );
        $show = ( empty( $perpage ) || $perpage == 0 ) ? 10: $perpage;

        $this->params['member_count'] = Member::count();
        $this->params['logs_count'] = MemberLog::count();

        if ($memberid) {

            $this->params['logs'] = MemberLog::where('member_id', $memberid)->paginate( $show );

        } else {

            $this->params['logs'] = MemberLog::paginate( $show );
        }

        $this->params['perpage'] = $show;
        $this->params['memberid'] = $memberid;

        return view('members.logs', $this->params);
    }

    public function print_report( Request $request )
    {
        $perpage = preg_replace('/\D/', '', $request->input('show') );
        $show = ( empty( $perpage ) || $perpage == 0 ) ? 10: $perpage;
        
        $start = date( 'Y-m-d', strtotime( $request->input('start_date') ) );
        $end = date( 'Y-m-d', strtotime( $request->input('end_date') ) );

        $this->params['start'] = '';
        $this->params['end'] = '';

        $this->params['logs'] = MemberLog::whereBetween('created_at', [$start, $end])->get();
        $this->params['start'] = date( 'F d, Y', strtotime( $request->input('start_date') ) );
        $this->params['end'] = date( 'F d, Y', strtotime( $request->input('end_date') ) );
    

        return view('layouts.print', $this->params);           
    }

    public function daily_logs()
    {
        return view('daily_logs');
    }
}
